module.exports = {
  "extends": "standard",
  'env': {
    'browser': true,
    'es6': true,
    'node': true,
    'mocha': true
  },
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'project': 'tsconfig.json',
    'sourceType': 'module'
  },
  'plugins': [
    '@typescript-eslint',
    '@typescript-eslint/tslint',
    'no-null',
    'filenames',
    'chai-friendly'
  ],
  'rules': {
    'semi': 'error',
    'space-before-function-paren': 'off',
    'no-unused-vars': 'off',
    'no-unused-expressions': 'off',
    'chai-friendly/no-unused-expressions': 'error',
    '@typescript-eslint/class-name-casing': 'error',
    '@typescript-eslint/indent': ['error', 2],
    '@typescript-eslint/member-delimiter-style': [
      'error',
      {
        'multiline': {
          'delimiter': 'none',
          'requireLast': true
        },
        'singleline': {
          'delimiter': 'semi',
          'requireLast': false
        }
      }
    ],
    '@typescript-eslint/prefer-namespace-keyword': 'error',
    '@typescript-eslint/quotes': [
      'error',
      'single',
      {
        'avoidEscape': true
      }
    ],
    '@typescript-eslint/semi': [
      'error',
      'never'
    ],
    '@typescript-eslint/type-annotation-spacing': 'error',
    'eol-last': 'error',
    'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
    'no-null/no-null': 'error',
    'no-return-await': 'error',
    'no-trailing-spaces': 'error',
    'filenames/match-exported': 'error',
    'no-var': 'error',
    'prefer-const': 'error',
    'spaced-comment': 'error',
    '@typescript-eslint/tslint/config': [
      'error',
      {
        'rules': {
          'jsdoc-format': true,
          'one-line': [
            true,
            'check-open-brace',
            'check-whitespace'
          ],
          'whitespace': [
            true,
            'check-branch',
            'check-decl',
            'check-operator',
            'check-module',
            'check-separator',
            'check-type'
          ]
        }
      }
    ]
  }
}
