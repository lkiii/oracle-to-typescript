# oracle-to-typescript

Generate TypeScript interfaces from Oracle Database types.

## Install

Locally
```
npm i -D oracle-to-typescript
```
Globally
```
npm i -g oracle-to-typescript
```
## Start

If installed Globally
```
otts
```

If installed Locally
1. Add run sript to your `package.json`
```
 "scripts": {
     .....,
    "otts": "otts"
}
```
2. Run with
```
npm run otts
```
