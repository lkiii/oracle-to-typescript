import IConfig from 'interfaces/IConfig'
import oracledb = require('oracledb')

export default class Oracle {
  private _connection: oracledb.Connection
  private _interfaces: string[] = []

  public async connect(config: IConfig) {
    this._connection = await oracledb.getConnection(config)
  }

  public async close() {
    this._connection.close()
  }

  public getType(name: string) {
    return this._connection.getDbObjectClass(name.toUpperCase())
  }

  public parseType(oracleType: oracledb.DBObjectClass) {
    if (oracleType === undefined) {
      throw new Error('Cannot parse undefined oracle type.')
    }
    const interfaceName = `I${oracleType.prototype.name}`
    const isArray = oracleType.prototype.isCollection
    const values: string[] = []
    const attributes = (isArray) ? oracleType.prototype.elementTypeClass.prototype.attributes : oracleType.prototype.attributes
    for (const index in attributes) {
      const attribute = attributes[index]
      let type = this.getAttributeType(attribute)
      if (type === undefined) {
        this.parseType(attribute.typeClass)
        type = `I${attribute.typeName.substring(attribute.typeName.indexOf('.') + 1)}`
      }
      values.push(`${index}: ${type}`)
    }
    let parameters = `\n  ${values.join('\n  ')}\n`
    let interfaceHeader = interfaceName
    if (isArray) {
      interfaceHeader += ` extends Array<{${parameters}}>`
      parameters = ' '
    }
    this._interfaces.push(`export interface ${interfaceHeader} {${parameters}}\n`)
    return {
      interfaceName,
      interfaceContent: this._interfaces.join('\n')
    }
  }

  private getAttributeType(attribute: {type: number; typeName: string}) {
    switch (attribute.type) {
      case oracledb.DB_TYPE_CHAR:
      case oracledb.DB_TYPE_VARCHAR:
      case oracledb.DB_TYPE_CLOB:
      case oracledb.DB_TYPE_LONG:
      case oracledb.DB_TYPE_NCHAR:
      case oracledb.DB_TYPE_NCLOB:
      case oracledb.DB_TYPE_NVARCHAR:
        return 'string'
      case oracledb.DB_TYPE_NUMBER:
      case oracledb.DB_TYPE_BINARY_DOUBLE:
      case oracledb.DB_TYPE_BINARY_FLOAT:
      case oracledb.DB_TYPE_BINARY_INTEGER:
        return 'number'
      case oracledb.DB_TYPE_DATE:
      case oracledb.DB_TYPE_TIMESTAMP:
      case oracledb.DB_TYPE_TIMESTAMP_LTZ:
      case oracledb.DB_TYPE_TIMESTAMP_TZ:
        return 'Date'
      case oracledb.DB_TYPE_BOOLEAN:
        return 'boolean'
      case oracledb.DB_TYPE_OBJECT:
        return undefined
      default:
        return 'any'
    }
  }
}
