import IConfig from 'interfaces/IConfig'
import { writeFileSync } from 'fs'
import { join } from 'path'
import pkgdir = require('pkg-dir')

export const checkConfig = (config: IConfig): boolean => {
  if (config === undefined) {
    return false
  }
  let configIsValid = true
  configIsValid = !(config.connectString === undefined || config.connectString.length < 1)
  configIsValid = !(config.password === undefined || config.password.length < 1)
  configIsValid = !(config.user === undefined || config.user.length < 1)
  return configIsValid
}

export const getConfigFilePath = async () => {
  const dir = await pkgdir()
  if (dir === undefined) {
    return dir
  }
  return join(dir, '.db.json')
}

export const saveInterface = (interfaces: { interfaceName: string; interfaceContent: string}) => {
  const path = join(process.cwd(), `${interfaces.interfaceName}.ts`)
  writeFileSync(path, interfaces.interfaceContent)
  console.log(`Interface file ${path} was successfully created.`)
}

export const saveConfig = (configFile: string, config: IConfig) => {
  try {
    writeFileSync(configFile, JSON.stringify(config))
  } catch (error) {
    console.error(`Writing config failed. ${error.message}`)
  }
}
