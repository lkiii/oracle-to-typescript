#!/usr/bin/env node
import { prompt } from 'inquirer'
import Oracle from './Oracle'
import IConfig from './interfaces/IConfig'
import { checkConfig, saveInterface, saveConfig, getConfigFilePath } from './'
import { readFileSync, existsSync } from 'fs'
import { DBObjectClass } from 'oracledb'

let config: IConfig
let configFile: string

const oracle = new Oracle()

const getType = async (oracle: Oracle): Promise<DBObjectClass> => {
  let oracleType: string
  try {
    ({ oracleType } = await prompt([
      { type: 'input', name: 'oracleType', message: 'Fill in Oracle Database type (leave empty to quit):' }
    ]))
    if (oracleType === '') {
      return undefined
    }
    const type = await oracle.getType(oracleType)
    return type
  } catch (error) {
    console.error(`Getting type "${oracleType}" failed. ${error.message}`)
    return getType(oracle)
  }
}

const work = async () => {
  configFile = await getConfigFilePath()
  if (existsSync(configFile)) {
    config = JSON.parse(readFileSync(configFile, 'utf8'))
  }
  if (!checkConfig(config)) {
    try {
      const dbConnectionAnswers = await prompt([
        { type: 'input', name: 'connectString', message: 'Fill in database connection string:' },
        { type: 'input', name: 'user', message: 'Fill in database user:' },
        { type: 'password', name: 'password', message: 'Fill in database password:' }
      ])
      config = dbConnectionAnswers
      if (configFile !== undefined) {
        const { save } = await prompt([
          { type: 'list', name: 'save', message: 'Would you like to save configuration to file?', choices: [{ name: 'Yes', value: true }, { name: 'No', value: false }], default: 0 }
        ])
        if (save === true) {
          saveConfig(configFile, config)
        }
      }
    } catch (error) {
      console.error(`Getting configuration input failed. ${error.message}`)
    }
  }

  try {
    await oracle.connect(config)
  } catch (error) {
    console.error(`Connect to database failed: ${error.message}`)
    return
  }
  const oracleType = await getType(oracle)
  if (oracleType !== undefined) {
    try {
      const tsInterfaces = oracle.parseType(oracleType)
      saveInterface(tsInterfaces)
    } catch (error) {
      console.error(`Creating interface failed. ${error.message}`)
    }
  }
  await oracle.close()
}

work()
