export default interface IConfig {
  user: string
  password: string
  connectString: string
}
